import pandas as pd
import click


@click.command()
@click.argument('input_path', type=click.Path())
@click.argument('output_path', type=click.Path())
def clean_data(input_path: str, output_path: str) -> None:
    """Clean data for representable view"""
    df = pd.read_csv(input_path, header=0)
    df.drop(['Unnamed: 32', 'id'], axis=1, inplace=True)
    binary = {'M': 1, 'B': 0}
    df['diagnosis'] = df['diagnosis'].map(binary)

    df.to_csv(output_path, index=False)


if __name__ == '__main__':
    clean_data()
